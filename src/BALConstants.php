<?php

namespace BreakawayLoyalty;

class BALConstants
{
    const URI_GET_BALANCE = '/getbalance';
    const URI_ADD_ACCOUNT = '/addaccount';
    const URI_ADD_POINTS = '/addpoints';
    const URI_REMOVE_POINTS = '/removepoints';
    const URI_REDEEM_POINTS = '/redeempoints';

    const PARAM_USERNAME = 'userName';
    const PARAM_PASSWORD = 'passWord';
    const PARAM_CUSTOMER_ID = 'customerId'; // preferably phone number
    const PARAM_REQUEST_TYPE = 'requestType';
    const PARAM_REQUEST_TYPE_A = 'A';   // add
    const PARAM_REQUEST_TYPE_R = 'R';   // remove
    const PARAM_REQUEST_TYPE_U = 'U';   // update
    const PARAM_ACCOUNT_LEVEL = 'accountLevel'; // multiplier for points (e.g. 1, 2, 3, 4)

    const PARAM_ADD_TYPE = 'addType';
    const PARAM_ADD_TYPE_POINTS = 'P';      // add points to balance
    const PARAM_ADD_TYPE_CALCULATE = 'C';   // ??
    const PARAM_REWARD_POINTS = 'rewardPoints';         // If addType=P
    const PARAM_TRANSACTION_AMOUNT = 'transactionAmt';  // If addType=C
    const PARAM_REWARD_PERCENTAGE = 'rewardPct';        // Id addType=C
    const PARAM_EARN_TYPE = 'earnType';
    const PARAM_EARN_TYPE_BASE = 'R';
    const PARAM_EARN_TYPE_BONUS = 'B';
    const PARAM_EARN_TYPE_VENDOR = 'V';
    const PARAM_EARN_TYPE_PROMO = 'P';
    const PARAM_VENDOR_ID = 'vendorId';     // BAL's internal ID for a partner/vendor
    const PARAM_ACTIVITY_ID = 'activityId'; // ID for activity which triggered adding of points (determined by vendor)
    const PARAM_POINTS_TO_REMOVE = 'pointsToRemove';

    const PARAM_FULL_NAME = 'name';
    const PARAM_DEVICE_ID = 'deviceId';         // IMEI
    const PARAM_STORED_VALUE_CARD = 'svCard';   // FIS card number
    const PARAM_REQUEST_ID = 'requestId';
}