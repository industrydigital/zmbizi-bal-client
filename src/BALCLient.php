<?php

namespace BreakawayLoyalty;

use BreakawayLoyalty\BALConstants;
use GuzzleHttp\Client;

/**
 * Client for Breakaway Loyalty
 * 
 * 
 */
class BALClient
{
    /**
     * Base URI
     *
     * @var string
     */
    private $baseUri = '';

    function __construct($baseUri)
    {
        $this->baseUri = $baseUri;
    }

    /**
     * Creates a BAL user account
     * 
     * @param string $jwt
     * @param string $json
     * @param string $requestId
     * 
     * @return Response
     */
    function addAccount($jwt, $json, $requestId)
    {
        $client = new Client(['base_uri' => $this->baseUri]);
        return $client->request(
            'POST',
            BALConstants::URI_ADD_ACCOUNT,
            $this->getHeaders($jwt, $requestId),
            $json
        );
    }

    /**
     * Fetches a user's Zmbizi points from BAL
     *
     * @param string $jwt
     * @param string $json  {}
     * @param string $requestId
     * 
     * @return Response
     */
    function getBalance($jwt, $json = '{}', $requestId)
    {
        $client = new Client(['base_uri' => $this->baseUri]);
        return $client->request(
            'GET', 
            BALConstants::URI_GET_BALANCE . "/$customerId", 
            $this->getHeaders($jwt, $requestId),
            $json
        );
    }

    /**
     * Adds points to a user's BAL account
     *
     * @param string $jwt
     * @param string $json
     * @param string $requestId
     * 
     * @return Response
     */
    function addPoints($jwt, $json, $requestId)
    {

        $client = new Client(['base_uri' => $this->baseUri]);
        return $client->request(
            'POST', 
            BALConstants::URI_ADD_POINTS, 
            $this->getHeaders($jwt, $requestId),
            $json
        );
    }

    /**
     * @param string $jwt
     * @param string $json
     * @param string $requestId
     * 
     * @return Response
     */
    function removePoints($jwt, $json, $requestId)
    {
        $client = new Client(['base_uri' => $this->baseUri]);
        return $client->request(
            'POST', 
            BALConstants::URI_REMOVE_POINTS, 
            $this->getHeaders($jwt, $requestId),
            $json
        );
    }

    /**
     * @param string $jwt
     * @param string $json
     * @param string $requestId
     * 
     * @return Response
     */
    function redeemPoints($jwt, $json, $requestId)
    {
        $client = new Client(['base_uri' => $this->baseUri]);
        return $client->request(
            'POST', 
            BALConstants::URI_REDEEM_POINTS,
            $this->getHeaders($jwt, $requestId),
            $json
        );
    }

    private function getHeaders($jwt, $requestId)
    {
        return [
            'Content-Type' => 'application/json',
            'X-Request-ID' => $requestId,
            'Authorization' => 'Bearer ' . $jwt
        ];
    }
}
